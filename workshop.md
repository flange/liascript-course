# Example LiaScript File

This is an example LiaScript File created by {your name here}.

# Main Content

Here is where we'll talk about the main topic.

Here's a link to the LiaScript site: [LiaScript](https://liascript.github.io/)

## Sub Point A

There might be several..

## Sub Point B

..*sections* of things..

## Sub Point C

.. we might want to talk about.

### Side Note

It all ends up in a **heierarchy**

# ^Second^ Piece of Content

And here's more. Note that the text formatting even works in the headers.